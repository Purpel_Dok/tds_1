// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDS_1GameMode.generated.h"

UCLASS(minimalapi)
class ATDS_1GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATDS_1GameMode();
};



